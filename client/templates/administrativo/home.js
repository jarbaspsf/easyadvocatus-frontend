Template.home.rendered = function(){
  $(".configBox").bootstrapSwitch({
    size: 'small'
  });
}

Template.home.helpers({
  'servicos' : function(){
    return Session.get('tipoServicos');
  },

  'configuracoes': function(){
    return Configs.find();
  },

  'checkConfig': function(config){
    return (config) ? "checked" : "";
  }
});

Template.home.events({
  'click .servicoValor': function(event){
    Session.set("servicoId", this.id);
    var data = {
      descricao: this.descricao
    }
    Modal.show('editarPrecoModal', data);
  },

  'switchChange.bootstrapSwitch .configBox': function(event, blazeEvent, state){
    Meteor.call("changeConfigValue", this._id, state, function(err, result){
      if(!err){
        
      }
    });
  }

})

Template.editarPrecoModal.rendered = function(){
  $('#valorServico').mask("#.##0,00", {reverse: true});
}

Template.editarPrecoModal.events({
  'submit #editarPrecoForm': function(event){
    event.preventDefault();

    var data = {
      id: Session.get("servicoId"),
      valor: Number($(event.target).find("[name=valorServico]").val().replace(/\./g, "").replace(/,/g,'.'))
    }


    Admin.spinShow();
    $.ajax({
      method: "POST",
      url: Meteor.settings.public.urlEndPoint+"/TipoServicos",
      type: "JSON",
      data: JSON.stringify(data),
      success: function(){
        $.ajax({
          method: "GET",
          url: Meteor.settings.public.urlEndPoint+"/TipoServicos",
          success: function(result){
            Session.set('tipoServicos', result);
            Admin.spinHide();
          }
        });
        Modal.hide('editarPrecoModal');
      },
      error: function(){
        Admin.spinHide();
        Modal.hide('editarPrecoModal');
      }
    });
  }
})
