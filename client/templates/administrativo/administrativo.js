Template.administrativo.rendered = function(){
  Session.setDefault("currentTemplate", "home");
}

Template.administrativo.events({
  'click .presentation' : function(event){
    event.preventDefault();
    var link = event.target;
    $(".presentation").removeClass("active");
    $(event.currentTarget).addClass("active");
    Session.set("currentTemplate", link.text.toLowerCase());
  }
});

Template.administrativo.helpers({
  currentTemplate: function(){
    return Session.get("currentTemplate");
  },

  loading: function(){
    return Session.get("promocoes") != null &&
      Session.get("despachantes") != null &&
      Session.get("tipoServicos") != null;
  }
});

Admin = {
  spinShow: function(){
    $("#spin-admin").show();
  },

  spinHide: function(){
    $("#spin-admin").hide();
  }
}
