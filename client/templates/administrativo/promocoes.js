Template.promocoes.rendered = function(){
  $(".previous").hide();
  Session.setDefault("search-promo", null);
  Promocoes.renderButtons();
}

Template.promoModal.rendered = function(){
  $('#editor').Editor({
    "insert_img":false,
    "togglescreen": false
  });
}

Template.promocoes.helpers({
  promocoes: function(){
    Promocoes.renderButtons();
    return Session.get("promocoes");
  }
});

Template.promocoes.events({
  'click .novaPromo': function(event){
    event.preventDefault();
    Modal.show('promoModal');
  },

  'click .next': function(event){
    event.preventDefault();
    Session.set("page-promocoes", Session.get("page-promocoes") + 1);
    Promocoes.refresh(Session.get("page-promocoes"));

  },

  'click .previous': function(event){
    event.preventDefault();
    Session.set("page-promocoes", Session.get("page-promocoes") - 1);
    Promocoes.refresh(Session.get("page-promocoes"));
  },

  'click .search': function(event){
    event.preventDefault();
    Session.set("search-promo", $("#buscar-promocao").val());
    Promocoes.refresh(1);
  },

  'click .refresh': function(event){
    event.preventDefault();
    Session.set("search-promo", null);
    Promocoes.refresh(1);
  }
})

Template.promoModal.events({
  'submit form': function(event){
    event.preventDefault();

    promocao = {
      titulo: $(event.target).find("[name=titulo]").val(),
      conteudo: $("#editor").Editor("getText"),
      usuarioid: 1
    }

    Modal.hide('promoModal');

    $.ajax({
      method: "POST",
      url: Meteor.settings.public.urlEndPoint+"/Promocoes",
      type: "JSON",
      data: JSON.stringify(promocao),
      success: function(){
          Session.set('search-promo', null);
          Promocoes.refresh(Session.get("page-promocoes"));
      }
    });
  }
})

Promocoes = {
  refresh: function(page){
    Admin.spinShow();
    Meteor.call("getPromocoes", page, Session.get("search-promo"), function(err,res){
      if(!err){
        Session.set('promocoes', res.data.listagem);
      }
      Admin.spinHide();
    })
  },

  renderButtons: function(){
    if(Session.equals("page-promocoes", 1)){
      $(".previous").hide();
    }else{
      $(".previous").show();
    }
    if(Session.get("promocoes").length < 12){
      $(".next").hide();
    }else{
      $(".next").show();
    }
  }
}
