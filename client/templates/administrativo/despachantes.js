Template.despachantes.rendered = function(){
  $(".previous").hide();
  Session.setDefault("search", null);
  Session.setDefault("tipoOrdemTexto", "Recentes  <span class='caret'></span>");
  Despachantes.renderButtons();
}

Template.despachantes.helpers({
  despachantes: function(){
    Despachantes.renderButtons();
    return Session.get("despachantes");
  },

  returnAuth: function(auth){
    if(auth)
      return "true";
    return "false";
  },

  tipoOrdem: function(){
    return Session.get("tipoOrdemTexto");
  }
});

Template.despachantes.events({
  'click .action': function(event){
    event.preventDefault();
    var values = $(event.target).parent().parent().attr("id").split("-");


    data = {
      id: values[0],
      autorizado: (values[1] == "true") ? 0 : 1
    };

    $.ajax({
      method: "POST",
      url: Meteor.settings.public.urlEndPoint+"/Despachantes/Autoriza",
      type: "JSON",
      data: JSON.stringify(data),
      success: function(){
        Session.set("search", null);
        Despachantes.refresh(Session.get("page-despachantes"));
      },
      error: function(err){
        console.log(err);
      }
    });

  },

  'click .tipoOrdem': function(event){
    event.preventDefault();
    Session.set("tipoOrdemTexto", $(event.target).text()+" <span class='caret'></span>");
    Session.set("tipoOrdem", $(event.target).text().toLowerCase().trim());
    Session.set("search", null);
    Despachantes.refresh(1);
  },

  'click .editarDespachante': function(event){
    event.preventDefault();

    var data = {
      user: this
    }
    Session.set("despachanteId", this.id);
    Modal.show('editarDespachante', data);
  },

  'keyup #search-despachantes': function(event){
    event.preventDefault();
    filter2(event.target.value, "search-despachantes");
  },

  'click .next': function(event){
    event.preventDefault();
    Session.set("page-despachantes", Session.get("page-despachantes") + 1);
    Despachantes.refresh(Session.get("page-despachantes"));

  },

  'click .previous': function(event){
    event.preventDefault();
    Session.set("page-despachantes", Session.get("page-despachantes") - 1);
    Despachantes.refresh(Session.get("page-despachantes"));
  },

  'click .search': function(event){
    event.preventDefault();
    Session.set("search", $("#buscar-despachante").val());
    Despachantes.refresh(1);
  },

  'click .refresh': function(event){
    event.preventDefault();
    Session.set("search", null);
    Despachantes.refresh(1);
  },

});

Template.editarDespachante.rendered = function(){
  Despachantes.editHelper();
}


Template.editarDespachante.events({
  'change #deleteDespachante': function(event){
    event.preventDefault();
    if($("#deleteDespachante").is(":checked")){
      $("#alertDelete").show();
    }else{
      $("#alertDelete").hide();
    }
  },

  'submit form': function(event){
    event.preventDefault();

    var data = {
      id: Session.get("despachanteId"),
      email:  $(event.target).find("[name=emailInput]").val(),
      telefone: $(event.target).find("[name=telefoneInput]").val().replace(/[^\w\s]/gi, '').replace(/ /g,''),
      deletado: $(event.target).find("[name=deleteInput]").is(":checked")
    }

    $.ajax({
      method: "POST",
      url: Meteor.settings.public.urlEndPoint+"/Despachantes",
      type: "JSON",
      data: JSON.stringify(data),
      success: function(){
        Despachantes.refresh();
        Modal.hide('editarDespachante');
      },
      error: function(){
        Despachantes.refresh();
        Modal.hide('editarDespachante');
      }
    });
  }
})


Despachantes = {
  refresh: function(page){
    Admin.spinShow();
    Meteor.call("getDespachantes", page, Session.get("search"), Session.get("tipoOrdem"),  function(err,res){
      if(!err){
        Session.set('despachantes', res.data.listagem);
      }
      Admin.spinHide();
    })
  },

  renderButtons: function(){
    if(Session.equals("page-despachantes", 1)){
      $(".previous").hide();
    }else{
      $(".previous").show();
    }
    if(Session.get("despachantes").length < 12){
      $(".next").hide();
    }else{
      $(".next").show();
    }
  },

  editHelper: function(){
    $("#telefoneInput").mask(Helpers.telefoneMask,{
      onKeyPress: function(val, e, field, options) {
        field.mask(Helpers.telefoneMask.apply({}, arguments), options);
      }
    });
  }
}
