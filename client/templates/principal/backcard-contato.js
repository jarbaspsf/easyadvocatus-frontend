Template.backcardContato.events({
  "submit #formContato": function(event){
    event.preventDefault();

    var data = {
      nome: $(event.target).find("[name=nomeContato]").val(),
      email: $(event.target).find("[name=emailContato]").val(),
      mensagem: $(event.target).find("[name=mensagemContato]").val(),
      recaptcha: $('#g-recaptcha-response').val()
    }

    $("#spinEmail").show();

    Meteor.call('formSubmissionMethodContato', data, function (error, result) {
        // recaptcha server response will be in result
        if(result.success != false){
          $.ajax({
            method: "POST",
            url: Meteor.settings.public.urlEndPoint+"/Jobs/Contato",
            type: "JSON",
            data: JSON.stringify(data),
            success: function(){
              $("#spinEmail").hide();
              var data = {
                message: "Seu email de contato foi enviado!",
                tittle: "Email Enviado!"
              }
              Modal.show('simpleModal', data);
              $("input").val("");
              $("textarea").val("");
              grecaptcha.reset();
              $('.card').removeClass('flipped');
            },
            error: function(){
              $("#spinEmail").hide();
              var data = {
                message: "O servidor encontrou um erro ao enviar o email, por favor tente novamente mais tarde",
                tittle: "Erro ao enviar o email"
              }
              Modal.show('simpleModal', data);
              grecaptcha.reset();
              $("input").val("");
              $("textarea").val("");
              $('.card').removeClass('flipped');
            }
          });
        }else{

        }
    });
  }
})
