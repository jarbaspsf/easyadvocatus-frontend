var formContainer;

Template.principal.rendered = function(){
  Session.setDefault("tipoServicoBotaoTexto",
    "Selecione o servico desejado <span class='caret'></span>");
  Session.setDefault("tipoServico", null);
  Session.setDefault("latitude", null);
  Session.setDefault("longitude", null);
  Session.setDefault("backcardTemplate", "backcard-thanks");
  Helpers.init();
}

Template.preloadpage.rendered = function(){
  $('.hide').show();
  if(Session.get("subseccaos") != null &&
    Session.get("tipoOABs") != null &&
    Session.get("tipoServicos") != null &&
    Session.get("numeroUsers") != null){
      $('body').toggleClass('loaded');
      $('#content').show();
      $('.navbar').show();
    }
}

Template.principal.helpers({
  tipoServicos: function(){
    return Session.get("tipoServicos");
  },

  backcardTemplate: function(){
    if(Session.get("backcardTemplate")){
      return Session.get("backcardTemplate");
    }else{
      return "backcard-thanks";
    }
  },

  checkTipos: function(tipo){
    if(Session.get("tipoServicoDescricao") == "Preposto"){
      if(tipo.descricao == "Outros")
        return true;
      return false;
    }else{
      if(tipo.descricao == "Outros")
        return false;
      return true;
    }
  },

  subseccaos: function(){
    return Session.get("subseccaos");
  },

  tipoOABs: function(){
    return Session.get("tipoOABs")
  },

  tipoServicoBotaoTexto: function(){
    return Session.get("tipoServicoBotaoTexto");
  },

  exibirUsers: function(){
    return Configs.findOne({nome: "SiteUsers"}).valor;
  },

  numeroUsers: function(){
    return 5133 + Session.get("numeroUsers");
  },

  load: function(){
    if(Session.get("subseccaos") != null &&
      Session.get("tipoOABs") != null &&
      Session.get("tipoServicos") != null &&
      Session.get("numeroUsers") != null){
        $('body').toggleClass('loaded');
        $('.navbar').show();
        $('#content').show();
      }
    }
});

Template.principal.events({
  'click #termosDeUso': function(event){
    Modal.show('termosModal');
  },

  "typeahead:selected .typeahead": function(event, object, dataset){
    Session.set('latitude', dataset.lat);
    Session.set('longitude', dataset.lng);
    //Helpers.setMap({lat: dataset.lat, lng: dataset.lng}, true);
  },

  'submit #jobForm': function(event){
    event.preventDefault();

    Session.set("backcardTemplate", "backcard-thanks");
    if(Session.get('tipoServico')){
      if(!$("#checkTermos").is(":checked")){
        var data = {
          message: "Por favor, leia e concorde com os termos de uso",
          tittle: "Termos de uso"
        }
        Modal.show('simpleModal', data);
        return;
      }

      if(Number($(event.target).find("[name=valorJob]").val().replace(/\./g, "").replace(/,/g,'.')) < Number(Session.get("valorMinimo"))){
        var data = {
          message: "Para o serviço utilizado deve-se informar um valor minimo de: "+accounting.formatMoney(Session.get("valorMinimo"), "R$ ", 2, ".", ","),
          tittle: "Valor minimo do serviço"
        }
        Modal.show('simpleModal', data);
        return;
      }

      var job = {
        nome: $(event.target).find("[name=nome]").val(),
        endereco: $(event.target).find("[name=enderecoTypeahead]").val() + " "+ $(event.target).find("[name=numero]").val() + " " +$(event.target).find("[name=complemento]").val(),
        telefone: $(event.target).find("[name=telefone]").val().replace(/[^\w\s]/gi, ''),
        numerooab: $(event.target).find("[name=oab]").val(),
        email: $(event.target).find("[name=email]").val(),
        valor: Number($(event.target).find("[name=valorJob]").val().replace(/\./g, "").replace(/,/g,'.')),
        tiposervicoid: Session.get('tipoServico'),
        latitude: Session.get('latitude'),
        longitude: Session.get('longitude'),
        tipooabid: parseInt($(event.target).find("[name=tipoOABs]").val()),
        subseccaoid: parseInt($(event.target).find("[name=subseccaos]").val())
      }

      if(!job.latitude || !job.longitude){
        var data = {
          message: "Selecione um dos endereços no campo de endereço",
          tittle: "Erro de Endereço"
        }
        Modal.show('simpleModal', data);
        return;
      }

      if(Session.get("tipoServicoTemHoras")){
        job.dt = moment($(".datetimepicker").val(), "DD/MM/YYYY hh:mm")._d.getTime();
      }else{
        job.dt = moment($(".datepicker").val(), "DD/MM/YYYY")._d.getTime();
      }

      $("#spin").show();
      $(".form-group").find("input").attr("disabled", "disabled");

      $.ajax({
        method: "POST",
        url: Meteor.settings.public.urlEndPoint+"/Jobs",
        type: "JSON",
        data: JSON.stringify(job),
        success: function(){
          Helpers.clearForm();
          $("#spin").hide();
          $(".form-group").find("input").attr("disabled", false);
          $('.card').addClass('flipped');
        },
        error: function(result){
          $("#spin").hide();
          $(".form-group").find("input").attr("disabled", false);
          var msg;
          (result.msg) ? msg = result.msg : msg = "Por favor, tente novamente mais tarde";
          var data = {
            message: msg,
            tittle: "Ocorreu um erro no servidor"
          }
          Modal.show('simpleModal', data);
        }
      });
    }else{
      var data = {
        message: "Por favor, escolha um tipo de servico para realizar a busca",
        tittle: "Solicitacao Incorreta"
      }
      Modal.show('simpleModal', data);
    }
  },

  'click .tipoServicoSelect': function(event){
    event.preventDefault();
    Session.set("tipoServicoDescricao", this.descricao);
    Session.set("tipoServicoBotaoTexto", this.descricao+" <span class='caret'></span>");
    Session.set("tipoServico", this.id);
    Session.set("tipoServicoTemHoras", this.temhoras);
    Session.set("valorMinimo", this.valor);
    if(!this.temhoras){
      $(".form-date").show();
      $(".form-datetime").hide();
    }else{
      $(".form-datetime").show();
      $(".form-date").hide();
    }

    if(this.descricao == "Preposto"){
      $("#oabInput").attr("required", false);
      $("#oabInput").attr("disabled", true);
    }else{
      $("#oabInput").attr("required", true);
      $("#oabInput").attr("disabled", false);
    }
  }
});

Helpers = {
  init : function(){
    $("#telefoneInput").mask(Helpers.telefoneMask,{
      onKeyPress: function(val, e, field, options) {
        field.mask(Helpers.telefoneMask.apply({}, arguments), options);
      }
    });

    $('#valorInput').mask('000.000.000.000.000,00', {reverse: true});

    $('.datepicker').datetimepicker({
      minDate: moment(),
      format: "DD/MM/YYYY",
      locale: moment().locale("pt-br")
    });

    $('.datetimepicker').datetimepicker({
      minDate: moment(),
      sideBySide: true,
      locale: moment().locale("pt-br")
    });

    $(".form-date").hide();

    $('.typeahead').typeahead({
        minLength: 6,
        highlight: true,
      },
    {
      name: 'my-dataset',
      source: function(query, callback){
        Helpers.getLocation(query, callback);
      },
    })

    //Helpers.setMap();

  },

  setMap: function(_location, marker){
    var location = {}
    if(!_location){
      location.lat = -9.546761;
      location.lng = -53.596665;
    }else{
      location = _location;
    }

    GoogleMaps.init(
      {
        'sensor': true, //optional
        'language': 'pt-br' //optional
      },
      function(){
        var zoom = 5;
        if(Session.get("latitude")){
          zoom = 16;
        }

        var mapOptions = {
          zoom: zoom,
          mapTypeId: google.maps.MapTypeId.MAP,
          disableDefaultUI: true
        };

        map = new google.maps.Map(document.getElementById("googlemaps"), mapOptions);
        map.setCenter(new google.maps.LatLng(location.lat, location.lng));

        if(Session.get("latitude")){
          var marker = new google.maps.Marker({
            position: new google.maps.LatLng(location.lat, location.lng)
          });
          marker.setMap(map);
        }
      }
    );
  },

  getLocation: function(address, cb){
    $("#enderecoLoading").removeClass("hide");
    Meteor.call("getGeoLocation", address, function(err, res){
      if(!err){
        if(cb){
          var locations = [];
          res.data.results.forEach(function(it){
            if(locations.length < 5 && !(it.types.indexOf('postal_code_prefix') != -1)){
             locations.push({value: it.formatted_address, lat: it.geometry.location.lat,
              lng: it.geometry.location.lng});
            }
          });
          $("#enderecoLoading").addClass("hide");
          cb(locations);
        }
      }
    });
  },

  telefoneMask: function (val) {
    return val.replace(/\D/g, '').length === 11 ? '(00) 00000-0000' : '(00) 0000-00009';
  },

  clearForm: function(){
    $("#jobForm").find("input[type=text]").val(null);
    $("#jobForm").find("input[type=email]").val(null);
    Session.set('tipoServico', null);
    Session.set("tipoServicoBotaoTexto",
    "Selecione o servico desejado <span class='caret'></span>");
  }
}
