Template.adminLogin.events({
  'submit form': function(event){
    event.preventDefault();
    $("#spin").show();

    var data = {
      email: $(event.target).find("[name=email]").val(),
      senha: $(event.target).find("[name=password]").val()
    }

    $.ajax({
      method: "POST",
      url: Meteor.settings.public.urlEndPoint+"/usuarios/fazlogin",
      type: "JSON",
      timeout: 30000,
      data: JSON.stringify(data),
      success: function(result){
        var user = {
          username: result.usuario.email.replace(" ", ""),
          password: data.senha,
          profile: {
            hash: result.hash,
            id: result.usuario.id
          }
        }

        Meteor.call("handleLogin", user.username, function(err, res){
          var username = result.usuario.email.replace(" ", "");
          if(!err){
            if(res){
              Meteor.loginWithPassword({username: username}, data.senha, function(err, res){
                if(!err){
                  Router.go("administrativo");
                }
                $("#spin").hide();
              });
            }else{
              Accounts.createUser(user, function(err, res){
                if(!err){
                  Meteor.loginWithPassword({username: username}, data.senha, function(err, res){
                    if(!err)
                      Router.go("administrativo");
                    $("#spin").hide();
                  });
                }
              });
            }
          }
        });
      },
      error: function(err){
        $("#spin").hide();
        console.log(err);
        var data = {
          message: "Por favor, tente novamente mais tarde",
          tittle: "Ocorreu um erro no servidor"
        }
        Modal.show('simpleModal', data);
      }
    });


  }
})
