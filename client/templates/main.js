Meteor.startup(function() {
  setWallpaper();
  reCAPTCHA.config({
      theme: 'light',  // 'light' default or 'dark'
      publickey: '6LfNtQUTAAAAANQooNcNa0EJId04qT-aoVjcBITj'
  });
});

setWallpaper = function(){
  $('html').addClass('full');
};

Meteor.subscribe("configs");
