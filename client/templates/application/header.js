Template.header.helpers({
  logout: function(){
    return Meteor.user();
  },

  adminRoute: function(){
    return Router.current().route.getName() == "administrativo";
  }
});

Template.header.events({
  'click .logout': function(event){
    event.preventDefault();
    Meteor.logout(function(err, res){
      if(!err)
        Router.go("principal");
    })
  },

  'click .portalAdmin': function(event){
    event.preventDefault();
    Router.go("administrativo");
  },

  'click #showContato': function(event){
    event.preventDefault();
    Session.set("backcardTemplate", "backcardContato");
    $('.card').addClass('flipped');
  },

  'click #goHome': function(event){
    event.preventDefault();
    if(Router.current().route.getName() != "principal")
      Router.go("principal");
    $('.card').removeClass('flipped');
  }

});
