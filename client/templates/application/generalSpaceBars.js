UI.registerHelper('money', function(money) {
  return accounting.formatMoney(money, "R$ ", 2, ".", ",");
});

UI.registerHelper('phone', function(phone) {
  if(phone.length > 9){
    return phone.slice(0, 2) + " " + phone.slice(2, phone.length);
  }else{
    return phone;
  }
});
