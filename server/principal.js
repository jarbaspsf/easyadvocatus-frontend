Meteor.methods({
  getTipoServicos: getTipoServicos,
  getPromocoes: getPromocoes,
  getDespachantes: getDespachantes,
  handleLogin: handleLogin,
  getGeoLocation: getGeoLocation,
  formSubmissionMethodContato: formSubmissionMethodContato,
  changeConfigValue: changeConfigValue
});

var urlEndPoint = Meteor.settings.public.urlEndPoint;

function getGeoLocation(address){
  this.unblock();
  try {
    var result = HTTP.call("GET", "https://maps.googleapis.com/maps/api/geocode/json?address="+address+"&language=pt-br&components=country:BR");
    return result;
  } catch (e) {
    return false;
  }

}

function getTipoServicos(){
  this.unblock();
  try {
    var result = HTTP.call("GET", urlEndPoint+"/TipoServicos");
      return result;
  } catch (e) {
    // Got a network error, time-out or HTTP error in the 400 or 500 range.
    return false;
  }
}

function getPromocoes(page, filter){
  this.unblock();
  var query = "?";
  if(page)
    query += "page="+page
  else
    query += "page=1";

  if(filter)
    query += "&titulo="+filter
  try {
    var result = HTTP.call("GET", urlEndPoint+"/Promocoes"+query);
    return result;
  } catch (e) {
    // Got a network error, time-out or HTTP error in the 400 or 500 range.
    return false;
  }
}

function getDespachantes(page, filter, tipoOrdem){
  this.unblock();
  var query = "?";
  if(page)
    query += "page="+page
  else
    query += "page=1";

  if(filter)
    query += "&nome="+filter

  if(tipoOrdem)
    query += "&tipoordem="+tipoOrdem;
  else
    query += "&tipoordem=recentes";

  try {
    var result = HTTP.call("GET", urlEndPoint+"/Despachantes"+query);
    return result;
  } catch (e) {
    // Got a network error, time-out or HTTP error in the 400 or 500 range.
    return false;
  }
}

function handleLogin(email){
  var userEmail = email.replace(" ", "");
  var user = Meteor.users.findOne({username: userEmail});
  if(!user){
    return false;
  }else{
    Meteor.users.update(user._id, {
      $set: {
        "profile.hash": user.hash
      }
    });

    return true;
  }
}

function changeConfigValue(id, valor){
  Configs.update(id, {$set : {
    valor: valor
  }});
}

function formSubmissionMethodContato(formData) {
    var verifyCaptchaResponse = reCAPTCHA.verifyCaptcha(
      this.connection.clientAddress, formData.recaptcha);

    if( verifyCaptchaResponse.data.success === false ){
        return verifyCaptchaResponse.data;
    }else{
      return {success: true}
    }
}
