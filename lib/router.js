Router.configure({
  layoutTemplate: "layout"
});

var urlEndPoint = Meteor.settings.public.urlEndPoint;

Router.map(function(){
  this.route('backcardConfirma',{
    path: '/pedidoconfirmado'
  });

  this.route('backcardNovaSenha',{
    path: '/senhagerada'
  });

  this.route('principal',{
    path: '/',
    waitOn: function(){
      return Meteor.subscribe("configs");
    },
    onBeforeAction: function(){
      HTTP.call("GET", urlEndPoint+"/TipoServicos",
      function(err, result){
        if(!err){
          Session.set("tipoServicos", result.data);
        }
      });

      HTTP.call("GET", urlEndPoint+"/Despachantes",
      function(err, result){
        if(!err){
          Session.set("numeroUsers", result.data.total);
        }
      });

      HTTP.call("GET", urlEndPoint+"/Subseccaos",
      function(err, result){
        if(!err){
          Session.set("subseccaos", result.data);
        }
      });

      HTTP.call("GET", urlEndPoint+"/TipoOABs",
      function(err, result){
        if(!err){
          Session.set("tipoOABs", result.data);
        }
      });
      this.next();
    }
  });

  this.route('adminLogin',{
    path: '/adminLogin'
  });

  this.route('administrativo',{
    path: '/administrativo',
    waitOn: function(){
      HTTP.call("GET", urlEndPoint+"/TipoServicos",
      function(err, result){
        if(!err){
          Session.set("tipoServicos", result.data);
        }
      });

      HTTP.call("GET", urlEndPoint+"/Despachantes?page=1&tipoordem=recentes",
      function(err, result){
        if(err){

        }else{
          Session.set("despachantes", result.data.listagem);
          Session.set("total-despachantes", result.data.total);
          Session.setDefault('page-despachantes', 1);
          HTTP.call("GET", urlEndPoint+"/Promocoes?page=1",
          function(err, result){
            if(err){

            }else{
              Session.set("total-promocoes", result.data.total);
              Session.setDefault('page-promocoes', 1);
              Session.set("promocoes", result.data.listagem);
            }
          });
        }
      });
    }
  });
});

var requireLogin = function() {
  if (! Meteor.user()) {
    if (Meteor.loggingIn()) {
      this.render(this.loadingTemplate);
    } else {
      this.render('accessDenied');
    }
  } else {
    this.next();
  }
}

Router.onBeforeAction(requireLogin, {only: 'administrativo'});
